<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'quienes-somos' ) ) ) : dynamic_sidebar( 'banner_quienes_somos' ); endif; ?>
				<?php if ( is_page( array( 'como-pensamos' ) ) ) : dynamic_sidebar( 'banner_como_pensamos' ); endif; ?>
				<?php if ( is_page( array( 'que-hacemos' ) ) ) : dynamic_sidebar( 'banner_que_hacemos' ); endif; ?>
				<?php if ( is_page( array( 'contacto' ) ) ) : dynamic_sidebar( 'banner_contacto' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->