<!-- Begin Content -->
	<section class="content" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 medium-9 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<?php the_title( '<h1>', '</h1>' ); ?>
						<p class="text-center"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?></p>
						<?php the_content(); ?>
					</article>
				<?php endwhile; endif; ?>
			</div>
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'right' ); ?>
			</div>
		</div>
	</section>
<!-- End Content -->